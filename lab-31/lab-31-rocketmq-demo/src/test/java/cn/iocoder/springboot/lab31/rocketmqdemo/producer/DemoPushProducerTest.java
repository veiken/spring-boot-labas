package cn.iocoder.springboot.lab31.rocketmqdemo.producer;

import cn.iocoder.springboot.lab31.rocketmqdemo.Application;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DemoPushProducerTest {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DemoPushProducer producer;

    @Test
    public void testSyncSend() throws InterruptedException {
        for (int i = 0; i < 1; i++) {
            int id = (int) (System.currentTimeMillis() / 1000) + i;
            SendResult result = producer.syncSend(id);
            logger.info("[testSyncSend][发送编号：[{}] 发送结果：[{}] {}]", id, result, result.getSendStatus());
        }

//         阻塞等待，保证消费
        new CountDownLatch(1).await();
    }

    @Test
    public void testASyncSend() throws InterruptedException {
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.asyncSend(id, new SendCallback() {

            @Override
            public void onSuccess(SendResult result) {
                logger.info("[testASyncSend][发送编号：[{}] 发送成功，结果为：[{}]]", id, result);
            }

            @Override
            public void onException(Throwable e) {
                logger.info("[testASyncSend][发送编号：[{}] 发送异常]]", id, e);
            }

        });

//         阻塞等待，保证消费
        new CountDownLatch(1).await();
    }

    @Test
    public void testOnewaySend() throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            int id = (int) (System.currentTimeMillis() / 1000);
            producer.onewaySend(id);
            logger.info("[testOnewaySend][发送编号：[{}] 发送完成]", id);
        }

        // 阻塞等待，保证消费
//        new CountDownLatch(1).await();
    }

}
