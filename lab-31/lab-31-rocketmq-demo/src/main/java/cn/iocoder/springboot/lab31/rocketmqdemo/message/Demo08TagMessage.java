package cn.iocoder.springboot.lab31.rocketmqdemo.message;

/**
 * 示例 07 的 Message 消息
 */
public class Demo08TagMessage {

    public static final String TOPIC = "test-topic008";

    /**
     * 编号
     */
    private Integer id;

    public Demo08TagMessage setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Demo08Message{" +
                "id=" + id +
                '}';
    }

}
