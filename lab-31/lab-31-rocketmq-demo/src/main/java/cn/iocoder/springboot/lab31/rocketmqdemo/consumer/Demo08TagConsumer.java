package cn.iocoder.springboot.lab31.rocketmqdemo.consumer;

import cn.iocoder.springboot.lab31.rocketmqdemo.message.Demo01Message;
import cn.iocoder.springboot.lab31.rocketmqdemo.message.Demo08TagMessage;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = Demo08TagMessage.TOPIC,
        consumerGroup = "test-group008",
        selectorExpression = "tag1 ||tag3"
)
public class Demo08TagConsumer implements RocketMQListener<Demo08TagMessage> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onMessage(Demo08TagMessage message) {
        logger.info("[Demo01Consumer：onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }

}
