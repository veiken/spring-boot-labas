package cn.iocoder.springboot.lab31.rocketmqdemo.consumer;

import cn.iocoder.springboot.lab31.rocketmqdemo.producer.DemoPushProducer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/** 测试消息Rocketmq的 消息重试机制
 * 不能使用 注解方式，因为没有返回值
 * 返回 RECONSUME_LATER 则会重新推送消息
 * todo 未解决问题： 返回 RECONSUME_LATER后，消息每隔10s推送一次，不是时间递增
 */
@Component
public class DataPushListener implements MessageListenerConcurrently {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private DemoPushProducer producer;

    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        log.info("consumeMessage coming size:{}", msgs.size());

        for (MessageExt msgExt : msgs) {
            try {
                String msg = new String(msgExt.getBody());
                log.info("push服务:DataPushListener->consumeMessage :{},getReconsumeTimes:{}", msg, msgExt.getReconsumeTimes());
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            } catch (Exception e) {
                log.error("DataPushListener-consumeMessage-exception:{}", e);
                if (msgExt.getReconsumeTimes() > 2) {
                    continue;
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
