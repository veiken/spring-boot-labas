package cn.iocoder.springboot.lab31.rocketmqdemo.message;

/**
 * 示例 01 的 Message 消息
 */
public class DemoPushMessage {

    public static final String TOPIC = "test-topicpush";

    /**
     * 编号
     */
    private Integer id;

    public DemoPushMessage setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Demo01Message{" +
                "id=" + id +
                '}';
    }

}
