package cn.iocoder.springboot.lab31.rocketmqdemo.producer;

import cn.iocoder.springboot.lab31.rocketmqdemo.message.DemoPushMessage;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemoPushProducer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public SendResult syncSend(Integer id) {

        // 创建 DemoPushMessage 消息
        DemoPushMessage message = new DemoPushMessage();
        message.setId(id);
        // 同步发送消息
        return rocketMQTemplate.syncSend(DemoPushMessage.TOPIC, message);
    }

    public void asyncSend(Integer id, SendCallback callback) {
        // 创建 DemoPushMessage 消息
        DemoPushMessage message = new DemoPushMessage();
        message.setId(id);
        // 异步发送消息
        rocketMQTemplate.asyncSend(DemoPushMessage.TOPIC, message, callback);
    }

    public void onewaySend(Integer id) {
        // 创建 DemoPushMessage 消息
        DemoPushMessage message = new DemoPushMessage();
        message.setId(id);
        // oneway 发送消息
        rocketMQTemplate.sendOneWay(DemoPushMessage.TOPIC, message);
    }

}
