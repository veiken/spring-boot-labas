package cn.iocoder.springboot.lab31.rocketmqdemo.producer;

import cn.iocoder.springboot.lab31.rocketmqdemo.message.Demo08TagMessage;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Demo08TagProducer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public SendResult syncSend(Integer id, String tag) {

        // 创建 Demo08TagMessage 消息
        Demo08TagMessage message = new Demo08TagMessage();
        message.setId(id);
        String topic = Demo08TagMessage.TOPIC +":"+tag;
//        System.out.println(topic);
        // 同步发送消息
        return rocketMQTemplate.syncSend(topic, message);
    }

    public void asyncSend(Integer id, SendCallback callback) {
        // 创建 Demo08TagMessage 消息
        Demo08TagMessage message = new Demo08TagMessage();
        message.setId(id);
        // 异步发送消息
        rocketMQTemplate.asyncSend(Demo08TagMessage.TOPIC, message, callback);
    }

    public void onewaySend(Integer id) {
        // 创建 Demo08TagMessage 消息
        Demo08TagMessage message = new Demo08TagMessage();
        message.setId(id);
        // oneway 发送消息
        rocketMQTemplate.sendOneWay(Demo08TagMessage.TOPIC, message);
    }

}
